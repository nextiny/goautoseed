// The package contains init() function that automatically initializes
// the default source of math/rand package by randomized seed value
// during each application launch.
//
// If you don't call package functions directly, you should import it
// using an empty alias:
//
//		import (
//			_ "path/to/goautoseed"
//		)
//
package goautoseed

import (
	"math/rand"
	"os"
	"time"
)

func init() {
	Init()
}

// The function initializes the default source of math/rand package
// by randomized seed value. It will be called automatically from
// the package initialization function init(), but it
// still can be called directly.
func Init() {
	rand.Seed(GenerateSeed())
}

// The function generates randomized seed value based on several
// random values such as the current time and the current process
// system IDs.
func GenerateSeed() int64 {
	// Note from math/rand: seed values that have the same
	// remainder when divided by 0x7fffffff == 2^31-1 generate
	// the same pseudo-random sequence.
	//
	// So, we can use any good randomized int32 as seed value.

	nanoTime := time.Now().UnixNano()

	/*

		// additional mask based on PID/ParentPID pair
		randMask := (os.Getpid()<<16) ^ (os.Getppid() ^ 0xffff)

		// seed value
		seedVal := (nanoTime & 0xffffffff) ^ (nanoTime>>32) ^ int64(randMask)

		// debug info
		log.Printf("PID %x; PPID %x",os.Getpid(),os.Getppid())
		log.Printf("time: %x, mask: %x, seed: %x", nanoTime, randMask, seedVal)

		rand.Seed(seedVal)
	*/

	return (nanoTime & 0xffffffff) ^ (nanoTime >> 32) ^ int64((os.Getpid()<<16)^(os.Getppid()^0xffff))

}
